import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;



public class SimpleFormJava extends Application {

	public static void main(String[] args) {
		launch(args);
		// TODO Auto-generated method stub

	}

	@Override
	public void start(Stage arg0) throws Exception {
		// TODO Auto-generated method stub
		// -----------------------------------------------
				// 1. Create & configure user interface controls
				// -----------------------------------------------
				
				// name label
				Label nameLabel = new Label("Enter your name");
				 
				// name textbox
				TextField nameTextBox = new TextField();
				
				// button
				Button goButton = new Button();
				goButton.setText("Click me!");
				
				// -----------------------------------------------
				// 2. Make a layout manager
				// -----------------------------------------------
				VBox root = new VBox();
				
				// -----------------------------------------------
				// 3. Add controls to the layout manager
				// -----------------------------------------------
				
				// add controls in the same order you want them to appear
				root.getChildren().add(nameLabel);
				root.getChildren().add(nameTextBox);
				root.getChildren().add(goButton);
				
				// -----------------------------------------------
				// 4. Add layout manager to scene
				// 5. Add scene to a stage
				// -----------------------------------------------

				// set the height & width of app to (300 x 250);
				arg0.setScene(new Scene(root, 1000, 250));
				
				// setting the title bar of the app
				arg0.setTitle("Example 01");
				
				// -----------------------------------------------
				// 6. Show the app
				// -----------------------------------------------
				arg0.show();
				goButton.setOnAction(new EventHandler<ActionEvent>() {
				    @Override
				    public void handle(ActionEvent e) {
				        // Logic for what should happen when you push button
                  
                   
                   String name = nameTextBox.getText();
   				
   				System.out.println("Hello " + name);
   				
   				nameTextBox.setText("");
   		
                   
				    }
				});
				
				
		
	}

}
